# Messenger serializers

A MessengerSerializer implementation which enables classes to be mapped to an eventName for improved interoperability.
