<?php

declare(strict_types=1);

namespace Tests;

use NigDevteam\MappedSerializer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\SerializerStamp;

class MappedSerializerTest extends TestCase
{
    public function testFullCircle(): void
    {
        $serializer = new MappedSerializer(new ForwardingSerializer(), [
            From::class => 'newElementEncoded'
        ], [
            'newElementEncoded' => To::class
        ]);

        $serializedEnvelope = $serializer->encode(
            (new Envelope(new From("12345"), [new CustomStamp1()]))
                ->with(new CustomStamp2())
        );

        $previousObject = $serializer->decode($serializedEnvelope)->getMessage();

        self::assertInstanceOf(To::class, $previousObject);

        self::assertSame('12345', $previousObject->secret);
    }

    public function testPartialDecode(): void
    {
        $serialized = ["body" => json_encode(["secret" => "12345"], JSON_THROW_ON_ERROR)];

        $serializer = new MappedSerializer(new ForwardingSerializer(), [], ['event' => To::class], 'event');

        $previousObject = $serializer->decode($serialized)->getMessage();

        self::assertInstanceOf(To::class, $previousObject);

        self::assertSame('12345', $previousObject->secret);
    }

    public function testErrorInUnknownClassToSerialize(): void
    {
        $serializer = new MappedSerializer(new ForwardingSerializer(), [
            From::class => 'newElementEncoded'
        ], [
            'newElementEncoded' => To::class
        ]);

        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage(
            "Impossible to extract mapping for \Tests\CustomStamp1 "
            . "because it is not in encodeMapping (It is not in decodeMapping either).\n"
            . "Try adding the missing mapping to the encodeMapping field in the configuration"
        );
        $serializer->encode(
            (new Envelope(new CustomStamp1(), [new CustomStamp1()]))
                ->with(new CustomStamp2())
        );
    }

    public function testEncodeDrawsFromDecode(): void
    {
        $serializer = new MappedSerializer(new ForwardingSerializer(), [
            From::class => 'newElementEncoded'
        ], [
            'newElementEncoded' => To::class
        ]);

        $result = $serializer->encode(
            (new Envelope(new To('12345'), [new CustomStamp1()]))
                ->with(new CustomStamp2())
        );

        self::assertEquals('{"secret":"12345"}', $result['body']);
    }

    public function testDecodeDoesNotDrawFromEncode(): void
    {
        $serialized = [
            "headers" => ["stamps" => serialize([new SerializerStamp(["eventName" => "event"])])],
            "body" => json_encode(["secret" => "12345"], JSON_THROW_ON_ERROR)];

        $serializer = new MappedSerializer(new ForwardingSerializer(), [To::class => 'event'], [], null);

        self::expectException(\LogicException::class);
        self::expectExceptionMessage("Impossible to extract mapping for event because"
            . " it is not in decodeMapping (It is in encodeMapping).
Try adding the missing mapping to the decodeMapping field in the configuration");
        $serializer->decode($serialized)->getMessage();
    }

    public function testNonSendableStampIsNotSent(): void
    {
        // Given
        $serializer = new MappedSerializer(new ForwardingSerializer(), [
            From::class => 'newElementEncoded'
        ], [
            'newElementEncoded' => To::class
        ]);

        // When
        $serializedEnvelope = $serializer->encode(
            (new Envelope(new From("12345"), [new CustomNonSendableStamp()]))
        );

        $decodedEnvelop = $serializer->decode($serializedEnvelope);

        // Then
        $previousObject = $decodedEnvelop->getMessage();

        self::assertInstanceOf(To::class, $previousObject);

        self::assertSame('12345', $previousObject->secret);

        self::assertStringNotContainsString(
            'CustomNonSendableStamp',
            print_r(
                $serializedEnvelope['headers'],
                true
            )
        );
    }
}
